package honcharuk.julia.restapi.services;

import honcharuk.julia.restapi.exceptions.NotFoundException;
import honcharuk.julia.restapi.models.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import honcharuk.julia.restapi.repositories.BookRepository;

import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public Optional<Book> getBookById(long id) {
        return bookRepository.findById(id);
    }

    /**Find book by id. If present, retrieve a book,
     * update by setting new title, author, year, genre
     * and save to DB. If a book isn't found, throw
     * NotFoundException.
     */
    public Book updateBook(long id, Book updatedBook) throws NotFoundException {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            Book book = optionalBook.get();
            book.setTitle(updatedBook.getTitle());
            book.setAuthor(updatedBook.getAuthor());
            book.setYear(updatedBook.getYear());
            book.setGenre(updatedBook.getGenre());
            return bookRepository.save(book);
        } else {
            throw new NotFoundException("Book not found");
        }
    }

    /**Find book by id. If it exists, it's deleted.
     * If not, throw NotFoundException.
     */
    public void deleteBook(long id) throws NotFoundException {
        Optional<Book> optionalBook = bookRepository.findById(id);
        if (optionalBook.isPresent()) {
            bookRepository.deleteById(id);
        } else {
            throw new NotFoundException("Book not found");
        }
    }
}
