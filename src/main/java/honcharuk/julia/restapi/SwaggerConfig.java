package honcharuk.julia.restapi;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("REST app for book management")
                        .version("1.0.0")
                        .description("Documentation on a simple book management application:\n" + "CRUD operations, MySQL DB, JUnit tested, conforming to SOLID and RESTful,\n" + "proper and thorough exception handling, neat HTTP codes and error info."));
    }
}
