package honcharuk.julia.restapi.controllers;

import honcharuk.julia.restapi.exceptions.NotFoundException;
import honcharuk.julia.restapi.models.Book;
import honcharuk.julia.restapi.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/books")
public class BookController {

    private final BookService bookService;

    //Injecting object dependency
    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    /**If there are any errors in the binding result, handle them and return the list of
     * error messages. If author and title fields are empty, return 400 status (Invalid input).
     * Otherwise, try to create the book, return created book if
     * successful or the exception message if not*/

    @PostMapping
    public ResponseEntity<?> createBook(@Valid @RequestBody Book book, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors()
                    .stream()
                    .map(error -> error.getDefaultMessage())
                    .collect(Collectors.toList());
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }
        if(book.getTitle().isEmpty() || book.getAuthor().isEmpty()) {
            return new ResponseEntity<>("Invalid input", HttpStatus.BAD_REQUEST);
        }
        try {
            Book createdBook = bookService.createBook(book);
            return new ResponseEntity<>(createdBook, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to create book", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public ResponseEntity<List<Book>> getAllBooks() {
        List<Book> books = bookService.getAllBooks();
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    /**Get a book from a BookService by id if it exists.
     * If a book is found, return it. Else return error message*/

    @GetMapping("/{id}")
    public ResponseEntity<?> getBookById(@PathVariable Long id) {
        Optional<Book> book = bookService.getBookById(id);
        if (book.isPresent()) {
            return new ResponseEntity<>(book.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Book not found", HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Update an existing book with the given ID.
     * If there are any errors in the binding result, handle them and return the list of error messages.
     * Otherwise, try to update the book, return updated book if successful, or appropriate error message if not.
     *
     * @param id     The ID of the book to be updated.
     * @param book   The updated book object.
     * @param result The binding result object to check for validation errors.
     * @return An HTTP response entity with the updated book or error message with appropriate status code.
     */

    @PutMapping("/{id}")
    public ResponseEntity<?> updateBook(@PathVariable Long id, @Valid @RequestBody Book book, BindingResult result) {
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors()
                    .stream()
                    .map(error -> error.getDefaultMessage())
                    .collect(Collectors.toList());
            return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
        }
        if (book.getTitle().isEmpty() || book.getAuthor().isEmpty()) {
            return new ResponseEntity<>("Invalid input", HttpStatus.BAD_REQUEST);
        }
        try {
            Book updatedBook = bookService.updateBook(id, book);
            if (updatedBook == null) {
                return new ResponseEntity<>("Book not found", HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(updatedBook, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to update book", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //Delete the book if found, return an error message
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteBook(@PathVariable Long id) {
        try {
            bookService.deleteBook(id);
            return new ResponseEntity<>("Book deleted successfully", HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>("Failed to delete book", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}