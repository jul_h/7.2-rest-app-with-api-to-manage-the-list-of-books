package honcharuk.julia.restapi.repositories;

import honcharuk.julia.restapi.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//Spring Data JPA Repository to perform DB operations for Book entity
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
}
