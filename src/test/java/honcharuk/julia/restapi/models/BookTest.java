package honcharuk.julia.restapi.models;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BookTest {
    private Book book;

    @BeforeEach
    public void setUp() {
        book = new Book();
    }
    @Test
    public void setIdValidIdSuccess() {
        Long id = 1L;
        book.setId(id);
        assertEquals(id, book.getId());
    }

    @Test
    public void setTitleValidTitleSuccess() {
        String title = "Title";
        book.setTitle(title);
        assertEquals(title, book.getTitle());
    }

    @Test
    public void setAuthorValidAuthorSuccess() {
        String author = "Author";
        book.setAuthor(author);
        assertEquals(author, book.getAuthor());
    }

    @Test
    public void setYearValidYearSuccess() {
        int year = 2022;
        book.setYear(year);
        assertEquals(year, book.getYear());
    }

    @Test
    public void setGenreValidGenreSuccess() {
        String genre = "Genre";
        book.setGenre(genre);
        assertEquals(genre, book.getGenre());
    }

    @Test
    public void constructorAllFieldsProvidedSuccess() {
        String title = "Title";
        String author = "Author";
        int year = 2022;
        String genre = "Genre";
        book = new Book(title, author, year, genre);

        assertEquals(title, book.getTitle());
        assertEquals(author, book.getAuthor());
        assertEquals(year, book.getYear());
    }

    @Test
    public void constructorNoFieldsProvidedSuccess() {
        book = new Book();

        assertNull(book.getTitle());
        assertNull(book.getAuthor());
        assertEquals(0, book.getYear());
    }
}

