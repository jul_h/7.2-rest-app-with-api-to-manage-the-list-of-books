package honcharuk.julia.restapi.services;

import honcharuk.julia.restapi.exceptions.NotFoundException;
import honcharuk.julia.restapi.models.Book;
import honcharuk.julia.restapi.repositories.BookRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class BookServiceTest {
    private BookRepository bookRepository;
    private BookService bookService;

    @BeforeEach
    public void setUp() {
        bookRepository = Mockito.mock(BookRepository.class);
        bookService = new BookService(bookRepository);
    }

    @Test
    public void testCreateBook() {
        Book book = new Book("Title", "Author", 2020, "Genre");

        when(bookRepository.save(any(Book.class))).thenReturn(book);

        Book createdBook = bookService.createBook(book);
        assertEquals(book, createdBook);
    }

    @Test
    public void testGetAllBooks() {
        List<Book> books = new ArrayList<>();
        books.add(new Book("Title1", "Author1", 2020, "Genre1"));
        books.add(new Book("Title2", "Author2", 2021, "Genre2"));

        when(bookRepository.findAll()).thenReturn(books);

        List<Book> allBooks = bookService.getAllBooks();
        assertEquals(2, allBooks.size());
        assertEquals(books, allBooks);
    }

    @Test
    public void testGetBookById() {
        long bookId = 1;
        Book book = new Book("Title", "Author", 2020, "Genre");

        when(bookRepository.findById(bookId)).thenReturn(Optional.of(book));

        Optional<Book> foundBook = bookService.getBookById(bookId);
        assertTrue(foundBook.isPresent());
        assertEquals(book, foundBook.get());
    }

    @Test
    public void testGetBookByIdNotFound() {
        long bookId = 1;

        when(bookRepository.findById(bookId)).thenReturn(Optional.empty());

        Optional<Book> foundBook = bookService.getBookById(bookId);
        assertFalse(foundBook.isPresent());
    }

    @Test
    public void testUpdateBook() throws NotFoundException {
        long bookId = 1;
        Book oldBook = new Book("Title", "Author", 2020, "Genre");
        Book updatedBook = new Book("New Title", "New Author", 2021, "New Genre");

        when(bookRepository.findById(bookId)).thenReturn(Optional.of(oldBook));
        when(bookRepository.save(any(Book.class))).thenReturn(updatedBook);

        Book updated = bookService.updateBook(bookId, updatedBook);
        assertEquals(updatedBook, updated);
        assertEquals(updatedBook.getTitle(), updated.getTitle());
        assertEquals(updatedBook.getAuthor(), updated.getAuthor());
        assertEquals(updatedBook.getYear(), updated.getYear());
        assertEquals(updatedBook.getGenre(), updated.getGenre());
    }

    @Test
    public void testUpdateBookNotFound() {
        long bookId = 1;
        Book updatedBook = new Book("Title", "Author", 2020, "Genre");

        when(bookRepository.findById(bookId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> bookService.updateBook(bookId, updatedBook));
    }

    @Test
    public void testDeleteBook() throws NotFoundException {
        long bookId = 1;
        Book book = new Book("Title", "Author", 2020, "Genre");

        when(bookRepository.findById(bookId)).thenReturn(Optional.of(book));

        assertDoesNotThrow(() -> bookService.deleteBook(bookId));
    }

    @Test
    public void testDeleteBookNotFound() {
        long bookId = 1;

        when(bookRepository.findById(bookId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> bookService.deleteBook(bookId));
    }
}
