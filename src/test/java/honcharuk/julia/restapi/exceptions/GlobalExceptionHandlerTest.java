package honcharuk.julia.restapi.exceptions;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class GlobalExceptionHandlerTest {

    @Mock
    private BindException bindException;

    @Mock
    private BindingResult bindingResult;

    @InjectMocks
    private GlobalExceptionHandler globalExceptionHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void handleValidationExceptionReturnsBadRequest() {
        // Arrange
        List<FieldError> fieldErrors = new ArrayList<>();
        when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);

        BindException bindException = new BindException(bindingResult);

        // Act
        ResponseEntity<?> response = globalExceptionHandler.handleValidationException(bindException);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(new ArrayList<>(), response.getBody());
    }

    @Test
    void handleValidationExceptionWithMethodArgumentTypeMismatchExceptionReturnsBadRequest() {
        // Arrange
        MethodArgumentTypeMismatchException ex = mock(MethodArgumentTypeMismatchException.class);

        // Act
        ResponseEntity<?> response = globalExceptionHandler.handleValidationException(ex);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Invalid parameter type", response.getBody());
    }

    @Test
    void handleBadRequestExceptionReturnsBadRequest() {
        // Arrange
        BadRequestException ex = new BadRequestException("Bad request");

        // Act
        ResponseEntity<?> response = globalExceptionHandler.handleBadRequestException(ex);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Bad request", response.getBody());
    }

    @Test
    void handleNotFoundExceptionReturnsNotFound() {
        // Arrange
        NotFoundException ex = new NotFoundException("Not found");

        // Act
        ResponseEntity<?> response = globalExceptionHandler.handleNotFoundException(ex);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Not found", response.getBody());
    }

    @Test
    void handleInternalServerErrorReturnsInternalServerError() {
        // Arrange
        Exception ex = new Exception("Internal server error");

        // Act
        ResponseEntity<?> response = globalExceptionHandler.handleInternalServerError(ex);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("Internal server error", response.getBody());
    }
}
