package honcharuk.julia.restapi.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BadRequestExceptionTest {

    @Test
    public void testBadRequestException() {
        String message = "Bad request";
        BadRequestException exception = new BadRequestException(message);
        assertEquals(message, exception.getMessage());
    }
}
